# Changes from Rebol2

There have been a large number of new words and syntax changes from Rebol2 and Rebol3

This is where we will document them.

## Branching Logic

* [If-else](if-else.md) (new)

## Datatypes

* none (deprecated) - see [_](underscore.md)
* [_](underscore.md) (new)
* [map!] (changed) - see [trello](https://trello.com/c/pWMjGYl7/93-select-and-map-key-operations-distinguish-mappings-to-blank-from-absence-of-a-mapping)

## Functions

* [--](--.md) (new
* [->](--gt.md) (new)
* ++ (deprecated) - see [me](me.md)
* [=>](eq-gt.md) (new)
* [Adapt](adapt.md) (new)
* Ajoin (deprecated) - see [spaced](./spaced.md), and [unspaced](./unspaced.md)
* [Chain](chain.md) (new)
* [Compose](compose.md) (changed
* [Default](default.md) (changed)
* [Did](did.md) (new)
* [Dump](dump.md) (new)
* [Elide](elide.md) (new
* [Enfix](enfix.md) (new)
* [Ensure](ensure.md) (new)
* [Hijack](hijack.md) (new)
* [Me](me.md) (new)
* [My](my.md) (new)
* [Nothing?](nothing-q.md) (new)
* [Read](read.md) (changed
* [Really](really.md) (new)
* Rejoin - see  [spaced](./spaced.md), and [unspaced](./unspaced.md)
* [Shove](shove.md) (new)
* [Something?](something-q.md) (new)
* [Source](source.md) (changed)
* [Spaced](spaced.md) (new)
* [Specialize](specialize.md) (new)
* [Unless](unless.md) (changed)
* [Unspaced](unspaced.md) (new)


## Function Changes

* [/local is now &lt;local&gt;](local.md) (changed)


## Looping Constructs

* [Every](every.md) (new) 
* [For-each](for-each.md) (new)
* Foreach (deprecated) - see [for-each](for-each.md)
 
:construction_worker:
