# Really

### USAGE:

REALLY value

### DESCRIPTION:

FAIL if value is null, otherwise pass it through  
REALLY is an ACTION!

### RETURNS: [any-value!]

## ARGUMENTS:

value [any-value!]

---

### Example

```
>> ensure [integer!] 1 + 1
== 2

>> ensure [decimal!] 1 + 1
** Error: ENSURE failed with argument of type integer!
** Where: fail _ ensure console
** Near: [
    fail [
        "ENSURE failed with argument of type"
 ...
```
