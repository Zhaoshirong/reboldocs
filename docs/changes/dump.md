# Dump

### USAGE:

DUMP :value :extra /prefix

### DESCRIPTION:

Show the name of a value or expressions with the value (See Also: --)
DUMP is an ACTION!

### RETURNS: (undocumented)

Doesn't return anything, not even void (so like a COMMENT)

### ARGUMENTS:

:value [any-value!]  
:extra [any-value! <...>]
    Optional variadic data for SET-WORD!, e.g. `dump x: 1 + 2`

### REFINEMENTS:

/prefix [text!]  
    Put a custom marker at the beginning of each output line
