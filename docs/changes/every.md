# Every

### USAGE:

EVERY 'vars data body

### DESCRIPTION:

Iterate and return false if any previous body evaluations were false  
EVERY is an ACTION!

### RETURNS: [<opt> any-value!]

null on BREAK, blank on empty, false or the last truthy value

### ARGUMENTS:

'vars [word! block!]  
    Word or block of words to set each time (local)  
data [<blank> any-series! any-context! map! datatype! action!]  
    The series to traverse  
body [<const> block! action!]  
    Block to evaluate each time
    
---

```
>> 	every a [_ 1][integer? a]
== #[false]
‌
‌>> 	every a [1 2][integer? a]
== #[true]
```
