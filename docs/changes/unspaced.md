# Unspaced

### USAGE:

UNSPACED line

### DESCRIPTION:

Joins a block of values into TEXT! with delimiters  
UNSPACED is an ACTION!

###RETURNS: [<opt> text!]

Null if blank input or block's contents are all null

### ARGUMENTS:

line [<blank> text! block!]  
    Will be copied if already a text value
    
see also [spaced](spaced.md)

---

Note how `none` or `_` is vapourised!

```
>> unspaced ["Is there a none?" if false ["no"] "here"]
== "Is there a none?here"
```

compared with in Rebol 2's `rejoin`

```
>> rejoin  ["Is there a none?" if false ["no"] "here"]
== "Is there a none?nonehere"
```

To get the same response in Rebol 2 you'd have to use `either`

```
>> rejoin  ["Is there a none?" either false ["no"][""] "here"]
== "Is there a none?here"
```
