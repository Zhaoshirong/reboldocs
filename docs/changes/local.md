# /local is now &lt;local&gt;

Functions previously used a `/local bar` parameter to indicate that `bar` was local to the function.

Now `<local> bar` is used reclaiming the name `local` for use.  Eg.  `<local> local` where `local` is now a local variable

Trello [card](https://trello.com/c/IyxPieNa/163-tags-in-function-spec-local-static-with-and-in-replace-and-enhance-refinements-with-external-from-r3-alpha)