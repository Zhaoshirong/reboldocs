# spaced

### USAGE:

SPACED line

### DESCRIPTION:

Joins a block of values into TEXT! with delimiters  
SPACED is an ACTION!

### RETURNS: [<opt> text!]

Null if blank input or block's contents are all null

### ARGUMENTS:
line [<blank> text! block!]  
    Will be copied if already a text value
    
See, also [unspaced](unspaced.md)
