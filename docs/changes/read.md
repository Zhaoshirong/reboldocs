# Read

`read` now returns only binary and needs to be converted to text if necessary.

In Rebol 2, you would do this

```
read http://www.rebol.com

or

to string! read/binary http://www.rebol.com
```

but in Ren-c the `/binary` refinement is gone.  It's now

```
to text! read http://www.rebol.com

or 

read/string http://www.rebol.com
```

where `read/string`  _Convert UTF and line terminators to standard text string_
