# For-each

### USAGE:

FOR-EACH 'vars data body 

## DESCRIPTION:

Evaluates a block for each value(s) in a series.  
FOR-EACH is an ACTION!

## RETURNS: [<opt> any-value!]

Last body result, or null if BREAK

## ARGUMENTS:

'vars [blank! word! 'word! block!]  
     Word or block of words to set each time, no new var if quoted  
data [<blank> any-series! any-context! map! any-path! action!]  
     The series to traverse  
body [<const> block! action!]  
    Block to evaluate each time
