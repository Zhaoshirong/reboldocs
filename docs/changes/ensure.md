# Ensure

### USAGE:

ENSURE 'test value

### DESCRIPTION:

Pass through value if it matches test, otherwise trigger a FAIL  
ENSURE is an ACTION!

### RETURNS: [<opt> any-value!]

Input if it matched, otherwise branch result

### ARGUMENTS:

'test [word! action! get-word! get-path! path! block! datatype! typeset! logic! tag! integer! quoted!]  
    Typeset membership, LOGIC! to test for truth, filter function  
value [<opt> any-value!]

