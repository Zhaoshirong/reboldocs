# Underscore

Underscore i.e. `_` replaces `none`

```
a: b: c: _

>> nothing? _
== #[true]

>> something? _
== #[false]
```
