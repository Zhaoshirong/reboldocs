# Shove

### USAGE:

SHOVE 'left :right /enfix /set

### DESCRIPTION:

Shove a parameter into an ACTION! as its first argument  
SHOVE is an ACTION!

### RETURNS: [<opt> any-value!]

### REVIEW: How might this handle shoving enfix invisibles?

### ARGUMENTS:

'left [<end> <opt> any-value!]  
    Requests parameter convention based on enfixee's first argument  
:right [<...> <end> any-value!]  
    (uses magic -- SHOVE can't be written easily in usermode yet)

## REFINEMENTS:

/enfix
    Follow completion rules for enfix, e.g. `1 + 2 <- * 3` is 9
/set
    If left hand side is a SET-WORD! or SET-PATH!, shove and assign

