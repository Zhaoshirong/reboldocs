# Compose

## USAGE:

COMPOSE :predicate :label value /deep /only

### DESCRIPTION:

Evaluates only contents of GROUP!-delimited expressions in an array
COMPOSE is an ACTION!

### RETURNS: [any-array! any-path! any-word! action!]

### ARGUMENTS:

:predicate [<skip> action!]  
    Function to run on composed slots (default: ENBLOCK)  
:label [<skip> tag! file!]  
    Distinguish compose groups, e.g. [(plain) (<*> composed)]  
value [any-array! any-path! any-word! action!]  
    Array to use as the template (no-op if WORD! or ACTION!)

### REFINEMENTS:

/deep  
    Compose deeply into nested arrays  
/only  
    Do not exempt ((...)) from predicate application  
    
---
Changes include the ability to use tags or % to specify which parenthesized lists you would like evaluated

```
>> compose <a> [(<b> print "phase B" 1 + 2) (3 + 4) (<a> print "phase A" 5 + 6)]
phase A
== [(<b> print "phase B" 1 + 2) (3 + 4) 11]
```

And in sequence

```
>> compose <b> compose <a> [(<b> print "phase B" 1 + 2) (3 + 4) (<a> print "phase A" 5 + 6)]
phase A
phase B
== [3 (3 + 4) 11]
```

Instead of a tag, you can use file! `%`

```
>> compose % [(1 + 2) (% 3 + 4) (% 5 + 6)]
== [(1 + 2) 7 11]
```

And composing on blocks in R2

``` 
>> block: [a b c]
== [a b c]
>> compose [(block) ((block))]
== [a b c a b c]
```

is now in R3

```
>> block: [a b c]
>> compose [(block) ((block))]
== [[a b c] a b c]
```
