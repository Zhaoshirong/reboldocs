# Specialize

### USAGE:

SPECIALIZE specializee def

### DESCRIPTION:

Create a new action through partial or full specialization of another
SPECIALIZE is an ACTION!

### RETURNS: [action!]

### ARGUMENTS:

specializee [action! word! path!]  
    Function or specifying word (keeps word for debug info)  
def [block!]  
    Definition for FRAME! fields for args and refinements