# Source

### USAGE:

SOURCE 'arg

### DESCRIPTION:

Prints the source code for an ACTION! (if available)
SOURCE is an ACTION!

### RETURNS: [void!]

### ARGUMENTS:

'arg [word! path! action! tag!]

---

You can also use `source` on a tag where the tag represents a community library

```
>> source <httpd>
https://raw.githubusercontent.com/r3n/renclib/master/modules/httpd.reb

>> source <send>
https://raw.githubusercontent.com/gchiu/Rebol3/master/protocols/prot-send.reb

```