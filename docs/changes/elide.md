# Elide

### USAGE:

ELIDE discarded

### DESCRIPTION:

Argument is evaluative, but discarded (see also COMMENT).  
ELIDE is an ACTION!

### RETURNS: (undocumented)

The evaluator will skip over the result (not seen, not even void)

### ARGUMENTS:

discarded [<opt> any-value!]  
    Evaluated value to be ignored.

---

## Examples

```
>> import <httpd>
== make module! [
    net-utils: [
        net-log _
    ]
    as-text: 'make action! [[binary] [...]]
]

>> elide import <httpd>

>>

>> print 1 + elide (print "hi") 2
hi
3

>> print 1 elide (print "hi") + 2
hi
** Script Error: + requires value1 argument
    to not be void

>> y: 1 + 2 elide (print y)
3
```

Trello [card](https://trello.com/c/snnG8xwW/173-elide-can-invisibly-evaluate-an-expression-almost-anywhere)
