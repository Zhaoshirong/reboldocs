# Unless

### USAGE:

left UNLESS right

### DESCRIPTION:

Variant of non-short-circuit OR which favors the right-hand side result  
UNLESS is an ACTION!

### RETURNS: [<opt> any-value!]

Conditionally true or false value (not coerced to LOGIC!)

### ARGUMENTS:

left [<opt> any-value!]  
    Expression which will always be evaluated  
right [<opt> any-value!]  
    Expression that's also always evaluated (can't short circuit)

---
Previously `unless` was a synonym for `if not`.  Now it's an OR which returns a value which is not forced to be a logical value

```
>> foo: (bar: 1) unless (bar: 2)
== 2

>> foo
== 2

>> foo: (bar: 1) unless (bar: false)
== 1

>> foo
== 1
```