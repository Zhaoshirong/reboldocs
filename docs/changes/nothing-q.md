# Nothing?

### USAGE:

NOTHING? value

### DESCRIPTION:

Returns TRUE if argument is either a BLANK! or NULL  
NOTHING? is an ACTION!

### RETURNS: (undocumented)

### ARGUMENTS:

value [<opt> any-value!]