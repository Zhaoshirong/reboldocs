# Chain

### USAGE:

CHAIN pipeline

### DESCRIPTION:
   
Create a processing pipeline of actions, each consuming the last result  
CHAIN is an ACTION!

### RETURNS: [action!]

### ARGUMENTS:

pipeline [block!]  
Block of ACTION!s to apply (will be LOCKed)