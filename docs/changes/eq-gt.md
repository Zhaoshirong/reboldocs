# =>

### USAGE:

:args => :body

### DESCRIPTION:

Convenience variadic wrapper for MAKE ACTION! or POINTFREE
=> is an ACTION!

### RETURNS: [action!]

### ARGUMENTS:

:args [<end> word! block!]  
    Block of argument words, or a single word (if only one argument)  
:body [any-value! <...>]  
    Block that serves as the body, or pointfree expression if no block

---

Example:

```
link: [href label] => [
    unspaced [{<a href="} href {" target="_blank">} label {</a>}]
]

>> link http://www.rebol.com "website"
== {<a href="http://www.rebol.com" target="_blank">website</a>}

```
