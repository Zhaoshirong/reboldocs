# Did

USAGE:

DID optional 

### DESCRIPTION:

Variant of TO-LOGIC which considers null values to also be false  
DID is an ACTION!

### RETURNS: [logic!]

true if value is NOT a LOGIC! false, BLANK!, or null

### ARGUMENTS:

optional [<opt> any-value!]

---

### Examples:

```
>> if did parse "ABC" ["A" any space "BC"] [print "parsed okay"]
parsed okay
```

