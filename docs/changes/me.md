# me

### USAGE:

'left ME :right

### DESCRIPTION:

Update variable using it as the left hand argument to an enfix operator  
ME is an ACTION!

### RETURNS: (undocumented)

### ARGUMENTS:

'left
:right

---

### Examples:

```
>> foo: 1
== 1

>> foo: me + 10
== 11
```

