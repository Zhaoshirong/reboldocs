# My

### USAGE:

'left MY :right

### DESCRIPTION:

Update variable using it as the first argument to a prefix operator  
MY is an ACTION!

### RETURNS: (undocumented)

### ARGUMENTS:
'left
:right

## REFINEMENTS:
/enfix
    
---

### Examples:

```
>> s: "hello"
== "hello"

>> s: my reverse
== "olleh"
```
