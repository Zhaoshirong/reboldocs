# Hijack

### USAGE:

HIJACK victim hijacker

### DESCRIPTION:

Cause all existing references to an ACTION! to invoke another ACTION!
HIJACK is an ACTION!

### RETURNS: [<opt> action!]

The hijacked action value, null if self-hijack (no-op)

### ARGUMENTS:

victim [action! word! path!]
    Action value whose references are to be affected.
hijacker [action! word! path!]
    The action to run in its place

---

### Example

We want to stop `read` from reading outside the current directory

```
; disable read outside current directory
old-read: copy :read
hijack 'read adapt 'old-read [
    if file? :source [
        source: clean-path source
        if not find source what-dir [
            fail "Not allowed to read outside the jail!"
        ]
    ]
]
```

Example taken from [eval](https://github.com/gchiu/rebolbot/blob/master/server/eval.reb) used for safe evaluation of a rebol word passed by CGI

Trello [card](https://trello.com/c/eaumDXoG/152-hijack-for-causing-another-function-to-run-in-place-of-a-previous-one)
