# Default

### USAGE:

:target DEFAULT 'branch :look

### DESCRIPTION:

Set word or path to a default value if it is not set yet or blank.  
DEFAULT is an ACTION!

### RETURNS: [<opt> any-value!]

Former value or branch result, can only be null if no target

### ARGUMENTS:

:target [<skip> set-word! set-path!]  
    Word or path which might be set--no target always branches  
'branch [block! action! quoted!]  
    If target not set already, this is evaluated and stored there  
:look [<...>]  
    Variadic lookahead used to make sure at end if no target

###REFINEMENTS:

/only
    Consider target being BLANK! to be a value not to overwrite

---

### Example

```
uri: default http://www.rebol.com
```

But if you're evaluating something, it needs to be in a block i.e. if it's not a literal like the URL above

    OSX64-latest: default [obj/name]
    
__default__ looks to its left to see if the set-word _uri:_ has a value or not.  If not, it then evaluates the block on the right and assigns the value to _uri_.  The original rebol3 behaviour was __default uri http://www.rebol.com__ which was less clear that an assignment was occuring.  One of the intended purposes was to setup some default values for a config file.

Alternate ways of doing the same thing:

    if blank? uri [uri: http://www.rebol.com]
    uri: any [uri http://www.rebol.com]
