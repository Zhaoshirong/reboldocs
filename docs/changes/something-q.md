# Something?

### USAGE:

SOMETHING? value

### DESCRIPTION:

Returns TRUE if a value is passed in and it isn't NULL or a BLANK!  
SOMETHING? is an ACTION!

### RETURNS: (undocumented)

### ARGUMENTS:

value [<opt> any-value!]