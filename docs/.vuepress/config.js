module.exports = {
    title: 'rebol-docs',
    description: 'Documentation Site For Rebol Ren-c',
    // using base of / since now have custom domain
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'icon', href: '/favicon.ico' }]
    ],
    // displays the UNIX timestamp(ms) of each file's last git commit at bottom 
    // of each page in an appropriate format
    // NOTE: only works if this is  git repo and there is at least 1 commit
    themeConfig: {
        repo: 'https://gitlab.com/Zhaoshirong/reboldocs',
        repoLabel: 'Edit!',
        // if your docs are in a different repo from your main project:
        docsRepo: 'https://gitlab.com/Zhaoshirong/reboldocs',
        // if your docs are not at the root of the repo:
        docsDir: 'docs',
        // if your docs are in a specific branch (defaults to 'master'):
        docsBranch: 'master',
        // defaults to false, set to true to enable
        editLinks: true,
        // custom text for edit link. Defaults to "Edit this page"
        editLinkText: 'Help us improve this page!',
        lastUpdated: 'Last Updated',
        nav: [
          { text: 'Home', link: '/' },
        ],
        //sidebar: 'auto'
        sidebar: [
            ['/about/', 'About'],
            ['/changes/', 'Changes'],
            ['/wasm/', 'Wasm']
        ]
    },
    plugins: ['@vuepress/plugin-last-updated']
}
