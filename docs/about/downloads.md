# Downloads

## Rebol3 Alpha

These were built by [Andreas Bolka](https://github.com/earl) from the main rebol [github repo](https://github.com/rebol/rebol) before he also followed Carl into the big cave of solitude

[Rebolsource](http://www.rebolsource.net)

## Ren-c Bootstrap Binaries

These are from December 2018, and use commit [8994d234](https://github.com/metaeducation/ren-c/commit/8994d234e8a8be2962a4045dc5b4ff4805d8ad61) 

* [LINUX 64](https://s3.amazonaws.com/r3bootstraps/r3-linux-x64-8994d23)
* [WINDOWS 32](https://s3.amazonaws.com/r3bootstraps/r3-windows-x86-8994d23.exe)
* [OSX](https://s3.amazonaws.com/r3bootstraps/r3-osx-x64-8994d23)
* [ANDROID](https://s3.amazonaws.com/r3bootstraps/r3-android-arm-8994d23)
* [OPENBSD](https://s3.amazonaws.com/r3bootstraps/r3-openbsd-x64-8994d23) (submitted by Stéphane Aulery) 

## Ren-c stable

Use these binaries for stable scripting

* Android
* Linux 32
* Linux 64
* iOS 64
* Windows 64

:construction_worker:

## Ren-c latest (experimental)

These are downloaded from a rebol console in browser!

Go to the [console](http://hostilefork.com/media/shared/replpad-js/)

and once it has loaded, type

```
do <downloads>
```

Then click on which platform you would like.
