# Brief Overview

## About Rebol

_Rebol is a modern interpreted language where code is data. It isn't object oriented, but has objects. It isn't a functional language but has first class functions. There are virtually no syntax rules or immutable keywords, making it ideal for developing domain-specific "dialects"._ - [Stackoverflow tag](https://stackoverflow.com/questions/tagged/rebol)

## Downloads

See [downloads](downloads.md)

## Discussion

A low activity [chat group](https://chat.stackoverflow.com/rooms/291/rebol) on stackoverflow

## Forum

A [discourse](https://discourse.org/) [forum](https://forum.rebol.info) discusses the language, and aims in more details

## Patreon

We have a [Patreon](https://www.patreon.com/rebolfoundation) account where people can contribute to help keep the [forum](https://github.com/rebol/rebol) going, and to pay for domain fees and data transfer fees.

## Porting Guide

Port Rebol2 code to Ren-c [Trello](https://trello.com/b/l385BE7a/rebol3-porting-guide-ren-c-branch)

## Repositories

Carl Sassenrath's original Github [repository](https://github.com/rebol/rebol) has been inactive since 2013.  Of historical interest only but you can build r3alpha from this.

Brian Dicken's [fork](https://github.com/hostilefork/rebol) known as ren-c is the main active branch and this site addresses the changes arising from that.
