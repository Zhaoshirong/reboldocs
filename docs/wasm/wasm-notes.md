# Random Notes

A place to collect notes before final organization


## Main

The [main](https://github.com/hostilefork/replpad-js/blob/master/main.reb#L58) function in [main.reb](https://github.com/hostilefork/replpad-js/blob/master/main.reb) is called from [gui.js](https://github.com/hostilefork/replpad-js/blob/master/gui.js#L715). This function is where you start your main program, and is an adaptation of the Rebol console.

## Download

Download the latest wasm and js files with this script

```
Rebol [
    file: %get-wasm.reb
    title: "Get WASM files"
    date: 17-Jan-2020
    notes: {downloads the latest rebol wasm/js files and saves them as
        libr3.js libr3.wasm libr3.worker.js
        
        which are the names used when serving from your own server
        
        use: rebol get-wasm.reb
    }
]

root: https://dd498l1ilnrxu.cloudfront.net/travis-builds/0.16.2/

hash: to text! read join root 'last-deploy.short-hash

for-each ext [%.js %.wasm %.worker.js][
    filename: to url! unspaced [root "libr3-" hash ext] 
    probe filename
    file: read filename    
    write to-file join "libr3" ext file
]
```

Author: <graham-chiu></graham-chiu>

<TestAlert display-text="Test" />
