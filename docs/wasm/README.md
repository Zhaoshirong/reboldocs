# Rebol WASM

## Demonstration

Run Rebol/ren-c in a [browser console](http://hostilefork.com/media/shared/replpad-js/)

When the console comes up, you can try

```
do <chess>

or 

do <downloads>
```

Brian Otto's demo GUI builder [here](http://rebol.brianotto.com/ui-builder/).  Type `run 'ui-builder`.  Background to how it works is in an [issue](https://github.com/hostilefork/replpad-js/issues/35) and [source](https://github.com/BrianOtto/ui-builder)

Watch Brian Otto's presentation from the Rebol Conference 2019

<iframe class="embed-responsive-item" src="https://youtube.com/embed/mjVtXfdTZIQ?autoplay=0&origin=https://rebol-docs.com" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!---
<iframe id="ytplayer" type="text/html" width="640" height="360"
  src="https://www.youtube.com/embed/mjVtXfdTZIQ?autoplay=0&origin=https://rebol-docs.com"
  frameborder="0"></iframe>
--->
  
<!--- https://developers.google.com/youtube/player_parameters --->

## Sources

### GitHub

[GitHub/Hostilefork - the console](https://github.com/hostilefork/replpad-js)

[GitHub/Metaeducation - rebol JS extension](https://github.com/metaeducation/ren-c/tree/master/extensions/javascript)

### APK

Giulio Lunati's [rebol-server](http://metaeducation.s3.amazonaws.com/travis-builds/rebol-server/rebol-server-4f67094-debug.apk) - Android apk demonstration using a modified rebol httpd, and WASM. [Source](https://github.com/metaeducation/rebol-server) 

Watch Giulio Lunati's presentation (in Italian with english subtitles) from the Rebol Conference 2019

<iframe class="embed-responsive-item" src="https://youtube.com/embed/r5kccBehMMg?autoplay=0&origin=https://rebol-docs.com" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!---
<iframe id="ytplayer" type="text/html" width="640" height="360"
  src="https://www.youtube.com/embed/r5kccBehMMg?autoplay=0&origin=https://rebol-docs.com"
  frameborder="0"></iframe>
-->

### Documentation

Is so far confined to the sources but here's a [cribsheet](https://github.com/hostilefork/replpad-js/wiki/Basic---Getting-started)

### Things to try

* [Chess](https://gitlab.com/Zhaoshirong/rebol-chess)
* [Downloads](https://dd498l1ilnrxu.cloudfront.net/code.reb)

### Minimal Self Hosting

If you would like to try running the rebol wasm on your own pages, and don't have the time to keep updating your code as changes occur, then it's best to store the various resources locally.  This includes the .js and .wasm files.

Here's an [example](https://zhaoshirong.gitlab.io/rebolwasm/) hosted on Gitlab.
Just clone the repository using this [link](https://gitlab.com/Zhaoshirong/rebolwasm.git) or

```
git clone git@gitlab.com:Zhaoshirong/rebolwasm.git
```

You can update the resources using this [download script](/wasm/wasm-notes.html#download)

### Notes

Use [this](wasm-notes.md) to make random notes
